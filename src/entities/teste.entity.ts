import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Teste {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({  length: 100, nullable: false })
    @ApiProperty()
    nome: string;

    @Column( {})
    @ApiProperty()
    idade: number;
}