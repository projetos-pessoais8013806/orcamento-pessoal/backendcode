import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { ApiBody, ApiProperty } from '@nestjs/swagger';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({ length: 100 })
  name: string;

  @ApiProperty()
  @Column({ length: 100 })
  email: string;

  @ApiProperty()
  @Column({ length: 100 })
  password: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  data_insercao: Date;
}

export class UserUpdate {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({ length: 100 })
  name: string;

  @ApiProperty()
  @Column({ length: 100 })
  email: string;

  @ApiProperty()
  @Column({ length: 100 })
  password: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  data_atualizacao: Date;
}

