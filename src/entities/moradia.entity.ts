import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate } from 'typeorm';

@Entity()
export class Moradia {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100, nullable: false })
  descricao: string;

  @Column('decimal', { precision: 6, scale: 2, nullable: true })
  real: number;

  @Column('decimal', { precision: 6, scale: 2 })
  planejado: number;

  @Column({ nullable: false })
  status: boolean;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  data_insercao: Date | null;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  data_atualizacao: Date | null;

  @BeforeInsert()
  setDataInsercao() {
    this.data_insercao = new Date();
  }

  @BeforeUpdate()
  setDataAtualizacao() {
    this.data_atualizacao = new Date();
  }
}
