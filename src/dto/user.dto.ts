import { ApiProperty, PickType } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsBoolean,
} from 'class-validator';

export class UserDto {
  @ApiProperty({
    type: 'string',
    description: 'Nome do usuario',
    example: 'João',
    required: true,
  })
  @IsString()
  name: string;

  @ApiProperty({
    type: 'string',
    description: 'E-Mail do usuario',
    example: 'joao@terra.com.br',
    required: true,
  })
  @IsString()
  email: string;

  @ApiProperty({
    type: 'string',
    description: 'Password do usuario',
    example: 'j213dsLds',
    required: true,
  })
  @IsString()
  password: string;
}
