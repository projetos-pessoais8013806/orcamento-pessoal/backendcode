import { ApiProperty, PickType } from '@nestjs/swagger'
import {
IsNotEmpty,
IsNumber,
IsOptional,
IsString,
IsBoolean,
} from 'class-validator'

export class MoradiaDto {
@ApiProperty({
    type: 'string',
    description: 'Descrição',
    example: 'Aluguel',
    required: true,
})
@IsString()
descricao: string

@ApiProperty({
    type: 'number',
    description: 'Gasto Real',
    example: '324.43',
    required: true,
})
@IsNumber()
real: number

@ApiProperty({
    type: 'number',
    description: 'Gasto planejado',
    example: '320.32',
    required: true,
})
@IsNumber()
planejado: number

@ApiProperty({
    type: 'boolean',
    description: 'Status',
    example: 'true',
    required: false,
})
@IsBoolean()
status: boolean
}
