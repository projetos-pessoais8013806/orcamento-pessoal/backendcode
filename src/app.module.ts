import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from './controllers/user.controller';
import { MoradiaController } from './controllers/moradia.controller';
import { TesteController } from './controllers/teste.controller';
import { User } from './entities/user.entity';
import { Moradia } from './entities/moradia.entity';
import { Teste } from './entities/teste.entity';

@Module({
  imports: [
    ConfigModule.forRoot(), // Carrega as variáveis de ambiente do arquivo .env
    TypeOrmModule.forRoot({
      type: process.env.TYPEORM_CONNECTION as 'postgres',
      host: process.env.TYPEORM_HOST,
      port: parseInt(process.env.TYPEORM_PORT),
      username: process.env.TYPEORM_USERNAME,
      password: process.env.TYPEORM_PASSWORD,
      database: process.env.TYPEORM_DATABASE,
      entities: [User, Moradia, Teste],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([User, Moradia, Teste]),
    // ... outros módulos
  ],
  controllers: [UserController, MoradiaController, TesteController],
  // ... providers, etc.
})
export class AppModule {}
