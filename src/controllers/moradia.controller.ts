import { Controller, Get, Post, Put, Delete, Body, Param, NotFoundException, Query, BadRequestException, InternalServerErrorException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Moradia } from "../entities/moradia.entity";
import { MoradiaDto } from "../dto/moradia.dto";
import { ApiBody, ApiOperation, ApiQuery } from '@nestjs/swagger';

@Controller('')
export class MoradiaController{
    constructor(
        @InjectRepository(Moradia)
        private readonly moradiaRepository: Repository<Moradia>,
    ) {}

    @Get('moradia')
    async findAllMoradia(): Promise<Moradia[]> {
        return await this.moradiaRepository.find();
    }

    @Post('moradia')
    @ApiOperation({ summary: 'Cria dados no setor moradia do orçamento' })
    @ApiBody({ type: MoradiaDto })
    async createMoradia(@Body() moradiaDto: MoradiaDto): Promise<Moradia> {
      try {
        const moradia = this.moradiaRepository.create(moradiaDto);
        const savedMoradia = await this.moradiaRepository.save(moradia);
        return savedMoradia;
      } catch (error) {
        throw new InternalServerErrorException(`Erro ao inserir dados em Moradia.`);
      }
    }

    @Put('moradia')
    @ApiOperation({ summary: 'Atualizando moradias' })
    @ApiBody({ type: MoradiaDto })
    @ApiQuery({ name: 'id', description: 'Id', required: true })
    async update(
      @Query('id') id: string,
      @Body() updateMoradiaDto: MoradiaDto,
    ): Promise<Moradia> {
      const parsedId = parseInt(id, 10);
      if (isNaN(parsedId)) {
        throw new BadRequestException('ID inválido fornecido');
      }
      const moradia = await this.moradiaRepository.preload({
        id: parsedId,
        ...updateMoradiaDto,
      });
      if (!moradia) {
        throw new NotFoundException(
          `ID do usuário ${parsedId} não encontrado.`,
        );
      }
      return this.moradiaRepository.save(moradia);
    }
}