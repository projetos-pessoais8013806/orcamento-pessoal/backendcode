import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  Query,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Raw, Repository } from 'typeorm';
import { User, UserUpdate } from '../entities/user.entity';
import {
  ApiProperty,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
} from '@nestjs/swagger';

import { UserDto } from '../dto/user.dto'; 

@Controller()
export class UserController {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  @Get('users')
  @ApiOperation({ summary: 'Retorna todos os usuários' })
  async findAllUsers(): Promise<User[]> {
    return await this.userRepository.find();
  }

  @Get('user')
  @ApiOperation({ summary: 'Retorna um usuario de acordo com o filtro' })
  @ApiQuery({ name: 'name', description: 'Nome', required: false })
  async findUserByName(@Query('name') nome: string): Promise<any> {
    const users = await this.userRepository.find({
      where: {
        name: Raw(alias => `${alias} LIKE '%${nome}%'`),
      },
    });
  
    if (!users || users.length ===  0) {
      throw new NotFoundException(`Nomes de usuários similares "${nome}" não encontrados.`);
    }
    return users;
  }

  @Post('user')
  @ApiOperation({ summary: 'Cria um usuário' })
  @ApiBody({ type: UserDto })
  async createUser(@Body() user: UserDto): Promise<UserDto> {
    const auxUser = await this.userRepository.save(user);
    if(!auxUser){
      throw new NotFoundException(`Erro ao inserir o usuário.`);
    }
    return user;
  }

  @Delete('user/:id')
  @ApiOperation({ summary: 'Deleta um usuário' })
  @ApiParam({ name: 'id', description: 'ID do usuário', required: true })
  async deleteUser(@Param('id') id: any): Promise<void> {
    const user = await this.userRepository.findOne({ where: { id: id } });
    if (!user) {
      throw new NotFoundException(`ID do usuário ${id} não encontrado.`);
    }
    await this.userRepository.remove(user);
  }

  @Put('user')
  @ApiOperation({ summary: 'Atualiza um usuário' })
  @ApiBody({ type: UserDto })
  @ApiQuery({ name: 'id', description: 'Id do usuário', required: true })
  async update(
    @Query('id') id: string,
    @Body() updateUser: UserUpdate,
  ): Promise<UserDto> {
    const parsedId = parseInt(id, 10);
    if (isNaN(parsedId)) {
      throw new BadRequestException('ID inválido fornecido');
    }
    const user = await this.userRepository.preload({
      id: parsedId,
      ...updateUser,
    });
    if (!user) {
      throw new NotFoundException(
        `ID do usuário #${parsedId} não encontrado.`,
      );
    }
    return this.userRepository.save(user);
  }
}
