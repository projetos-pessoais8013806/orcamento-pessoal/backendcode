import { Controller, Get, Post, Put, Delete, Body, Param } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Teste } from "../entities/teste.entity";
import { ApiBody } from '@nestjs/swagger';

@Controller('teste')
export class TesteController{
    constructor(
        @InjectRepository(Teste)
        private readonly moradiaRepository: Repository<Teste>,
    ) {}

    @Get()
    async findAllMoradia(): Promise<Teste[]> {
        return await this.moradiaRepository.find();
    }
}